import {Component, OnInit} from '@angular/core';
import {Task} from '../../task/task.model';
import {UserService} from '../../user/user.service';
import {TaskService} from '../../task/task.service';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  showTasks = 'executor';
  showedTasks: Array<Task>;

  constructor(public userService: UserService, public taskService: TaskService) {
    this.taskService.getTasksUserExecutor(this.userService.user._id).subscribe(response => {
      this.showedTasks = response.tasks;
    });
  }

  showExecutorTasks() {
    this.showTasks = 'executor';
    this.taskService.getTasksUserExecutor(this.userService.user._id).subscribe(response => {
      this.showedTasks = response.tasks;
    });
  }

  showCreatorTasks() {
    this.showTasks = 'creator';
    this.taskService.getTasksUserCreator(this.userService.user._id).subscribe(response => {
      this.showedTasks = response.tasks;
    });
  }

  ngOnInit() {

  }

}
