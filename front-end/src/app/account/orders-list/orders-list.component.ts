import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../order/order.service';
import {Order} from '../../order/order.model';
import {Observable} from 'rxjs/Observable';
import {enumUserTypes} from '../../user/user.model';
import {UserService} from '../../user/user.service';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit {
  userOrders: Array<Order>;
  allOrders: Array<Order>;
  userTypes = enumUserTypes;
  showUserOrders = 'opened';
  showOrders = 'opened';


  constructor(public userService: UserService, public orderService: OrderService) {
  }

  ngOnInit() {
    if (this.userService.user.userType === this.userTypes.user) {
      this.orderService.getUserOpenedOrders().subscribe(response => {
        this.userOrders = response;
      });
    }
    if (this.userService.user.userType === this.userTypes.employee ||
      this.userService.user.userType === this.userTypes.manager ||
      this.userService.user.userType === this.userTypes.root) {
      this.orderService.getAllOpenedOrders().subscribe(response => {
        this.allOrders = response;
      });
    }
  }

  showOpenedOrders() {
    this.showOrders = 'opened';
    this.orderService.getAllOpenedOrders().subscribe(response => {
      this.allOrders = response;
    });
  }

  showClosedOrders() {
    this.showOrders = 'closed';
    this.orderService.getAllClosedOrders().subscribe(response => {
      this.allOrders = response;
    });
  }

  showCanceledOrders() {
    this.showOrders = 'canceled';
    this.orderService.getAllCanceledOrders().subscribe(response => {
      this.allOrders = response;
    });
  }

  showUserOpenedOrders() {
    this.showUserOrders = 'opened';
    this.orderService.getUserOpenedOrders().subscribe(response => {
      this.userOrders = response;
    });
  }

  showUserClosedOrders() {
    this.showUserOrders = 'closed';
    this.orderService.getUserClosedOrders().subscribe(response => {
      this.userOrders = response;
    });
  }

}
