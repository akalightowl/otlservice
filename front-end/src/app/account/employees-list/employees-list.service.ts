import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../../user/user.model';
import {apiRoutes} from '../../app.config';

@Injectable()
export class EmployeesListService {

  constructor(private http: HttpClient) {

  }

  getEmployeesList(): Observable<User[]> {
    return this.http.get<User[]>(apiRoutes.employees);
  }
}
