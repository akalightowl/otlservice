import {Component, OnInit} from '@angular/core';
import {User} from '../../user/user.model';
import {EmployeesListService} from './employees-list.service';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.scss']
})
export class EmployeesListComponent implements OnInit {
  employees: User[];

  constructor(private employeesService: EmployeesListService) {
  }

  ngOnInit() {
    this.getUsersList();
  }

  getUsersList(): void {
    this.employeesService.getEmployeesList().subscribe(employees => this.employees = employees);
  }

}
