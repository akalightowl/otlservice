import {Component, OnInit} from '@angular/core';
import {User} from '../../user/user.model';
import {ClientsListService} from './clients-list.service';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
  clients: User[];

  constructor(private clientsService: ClientsListService) {
  }

  ngOnInit() {
    this.getClientsList();
  }

  getClientsList(): void {
    this.clientsService.getClientsList().subscribe(clients => this.clients = clients);
  }

}
