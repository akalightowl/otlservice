import {enumUserTypes} from '../user/user.model';

export class Message {
  constructor(
    public _id: string = null,
    public userId: string = null,
    public orderId: string = null,
    public isRead: boolean = false,
    public messageContent: string = null,
    public messageCreatorType: enumUserTypes = enumUserTypes.user,
    public messageDataCreated: Date = new Date()
  ) {
  }
}
