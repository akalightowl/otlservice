import {Injectable} from '@angular/core';
import {Message} from './message.model';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user/user.service';
import {Observable} from 'rxjs/Observable';
import {apiRoutes} from '../app.config';
import {OrderService} from '../order/order.service';

@Injectable()
export class MessageService {
  message: Message = new Message();
  messagesList: Array<Message> = [];
  cancelOrderForm = false;

  messageText = {
    className: 'messageContentFieldText' as string,
    label: 'Введите текст сообщения' as string,
    value: '' as string,
  };

  messageFromUser = true;

  constructor(private router: Router,
              private httpClient: HttpClient,
              public userService: UserService,
              public orderService: OrderService) {
  }


  getMessage(messageId: string): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.messages}/${messageId}`);
  }

  getOrderMessagesAndOrderInfo(orderId: string): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.orders}/${orderId}/messages`);
  }

  createMessage(orderId: string, messageContent: string): Observable<Message> {
    return this.httpClient.post<Message>(`${apiRoutes.messages}`,
      {
        userId: this.userService.user._id,
        orderId: orderId,
        isRead: this.message.isRead,
        messageDataCreated: this.message.messageDataCreated,
        messageCreatorType: this.userService.user.userType,
        messageContent: messageContent
      });
  }

  updateMessage(messageId: string, messageContent: string): Observable<Message> {
    return this.httpClient.put<Message>(`${apiRoutes.messages}/${messageId}`,
      {
        userId: this.userService.user._id,
        orderId: this.orderService.order._id,
        isRead: this.message.isRead,
        messageDataCreated: this.message.messageDataCreated,
        messageContent: messageContent
      });
  }

  deleteMessage(messageId: string): Observable<any> {
    return this.httpClient.delete<any>(`${apiRoutes.messages}/${messageId}`);
  }


}
