import {Component, OnInit} from '@angular/core';
import {UserService} from '../user/user.service';
import {OrderService} from '../order/order.service';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {MessageService} from './message.service';
import {enumUserTypes} from '../user/user.model';
import {NotificationService} from '../notification/notification.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  orderName: string;
  orderId: string;
  messageContent: string;
  messageContentUpdate: string;
  messageIdForUpdate: string;
  userTypes = enumUserTypes;
  edit = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public userService: UserService,
    public orderService: OrderService,
    public messageService: MessageService,
    public notificationService: NotificationService) {
  }

  ngOnInit() {
    // Get order from server using order ID. Need it because this module initial from routing after geting URL
    // with ID
    this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.messageService.getOrderMessagesAndOrderInfo(params.get('orderId'))).subscribe(response => {
      this.messageService.messagesList = response.messagesList;
      this.orderId = response.orderId;
      this.orderName = response.orderName;
    });
  }

  createMessage() {
    // Create new message and refresh messages list.
    this.messageService.createMessage(this.orderId, this.messageContent).subscribe(response => {
      this.route.paramMap
        .switchMap((params: ParamMap) =>
          this.messageService.getOrderMessagesAndOrderInfo(params.get('orderId'))).subscribe(response => {
        this.messageService.messagesList = response.messagesList;
      });
    });
  }

  cancelOrderAndCreateMessage(orderId: string) {
    // Create new message and cancel order.
    this.messageService.createMessage(this.orderId, this.messageContent).subscribe(response => {
      this.orderService.order.isOrderOpen = false;
      this.orderService.order.isOrderCancel = true;
      this.orderService.order.orderWorkStatus = 'Заказ отменен';
      this.messageService.cancelOrderForm = false;
      this.orderService.cancelOrder(orderId).subscribe(response => {
        this.notificationService.getNotification().subscribe(response => {
          this.router.navigate(['/account/orders-list']);
        });
      });
    });
  }

  getMessageForUpdating(messageId: string) {
    // Get message content for update message.
    this.messageService.getMessage(messageId).subscribe(response => {

      this.messageContentUpdate = response.messageContent;
      this.messageIdForUpdate = messageId;
    });
  }

  updateMessage(messageId: string) {
    // Update message and refresh messages list.
    this.messageService.updateMessage(messageId, this.messageContentUpdate).subscribe(response => {
      this.route.paramMap
        .switchMap((params: ParamMap) =>
          this.messageService.getOrderMessagesAndOrderInfo(params.get('orderId'))).subscribe(response => {
        this.messageService.messagesList = response.messagesList;
        this.orderId = response.orderId;
        this.orderName = response.orderName;
        this.closeEditionMessage();
      });
    });
  }

  deleteMessage(messageId: string) {
    // Delete message and refresh messages list.
    this.messageService.deleteMessage(messageId).subscribe(response => {
      this.route.paramMap
        .switchMap((params: ParamMap) =>
          this.messageService.getOrderMessagesAndOrderInfo(params.get('orderId'))).subscribe(response => {
        this.messageService.messagesList = response.messagesList;
        this.orderId = response.orderId;
        this.orderName = response.orderName;
      });
    });
  }


  editMessage(messageId: string) {
    this.edit = true;
    this.getMessageForUpdating(messageId);
  }

  closeEditionMessage() {
    this.edit = false;
  }

  closeOrderCancelForm() {
    this.messageService.cancelOrderForm = false;
  }
}
