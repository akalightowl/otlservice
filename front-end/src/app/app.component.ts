import {Component} from '@angular/core';
import {UserService} from './user/user.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';
import {NotificationService} from './notification/notification.service';
import {Notification} from './notification/notification.model';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/startWith';
import {transition, trigger, query, style, animate, group, animateChild} from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('routerTransition', [
      transition('* <=> *', [
        /* order */
        /* 1 */ query(':enter, :leave', style({position: 'fixed', width: '100%'})
          , {optional: true}),
        /* 2 */ group([  // block executes in parallel
          query(':enter', [
            style({opacity: '0', transform: 'scale(.96)'}),
            animate('135ms 220ms ease-in-out', style({transform: 'scale(1)', opacity: '.8'}))
          ], {optional: true}),
          query(':leave', [
            style({opacity: '1'}),
            animate('130ms ease-in-out', style({transform: 'scale(.96)', opacity: '.05'}))], {optional: true}),
        ])
      ])
    ])
  ]
})

export class AppComponent {
  route: string;

  constructor(public userService: UserService,
              public router: Router,
              public location: Location,
              public notificationService: NotificationService) {
    router.events.subscribe((val) => {
      if (location.path() !== '') {
        this.route = location.path();
      } else {
        this.route = '/';
      }
    });
    // Get notification
    // StartWith need for first start without interval
    // TODO: switch this on registration user. Not for all.
    IntervalObservable.create(6000).startWith(1).subscribe(response => {
      this.notificationService.getNotification().subscribe(notification => {
        this.notificationService.notification = notification;
      });
    });
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }

  onActivate(event) {
    console.log('On top');
    window.scroll(0, 0);
  }
}
