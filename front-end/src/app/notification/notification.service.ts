import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {apiRoutes} from '../app.config';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Notification} from './notification.model';

@Injectable()
export class NotificationService {
  notification: Notification = new Notification();

  constructor(private router: Router,
              private httpClient: HttpClient) {
  }

  getNotification(): Observable<Notification> {
    return this.httpClient.get<Notification>(apiRoutes.notifications);
  }

}
