import {Component, OnInit} from '@angular/core';
import {UserService} from '../user/user.service';
import {OrderService} from './order.service';
import {enumUserTypes} from '../user/user.model';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {Order} from './order.model';
import {Observable} from 'rxjs/Observable';
import {MessageService} from '../message/message.service';
import {NotificationService} from '../notification/notification.service';
import {apiRoutes} from '../app.config';
import {FileUploader} from 'ng2-file-upload';
import * as moment from 'moment';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  apiRoute = apiRoutes.fileDownload;
  orderContent = [];
  orderEdit: boolean;
  userTypes = enumUserTypes;
  uploader: FileUploader = new FileUploader({url: apiRoutes.fileUpload, autoUpload: true});
  hasBaseDropZoneOver = false;
  deletingOrders = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public userService: UserService,
    public orderService: OrderService,
    public messageService: MessageService,
    public notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    // Get order from server using order ID. Need it because this module initial from routing after geting URL
    // with ID
    this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.orderService.getOrder(params.get('orderId'))).subscribe(response => {

      this.orderService.order = response;
      // This code find className fields in orderContent for dynamic creating fields in class for using it in
      // ngModel and sending new order after editing. It code need for universal creating fields for different
      // order types with different fields.
      for (let item in this.orderService.order.orderContent) {
        if (this.orderService.order.orderContent.hasOwnProperty(item)) {
          let newClassFieldName: string = this.orderService.order.orderContent[item].className;
          let newClassFieldLabel: string = this.orderService.order.orderContent[item].label;
          let newClassFieldValue: string = this.orderService.order.orderContent[item].value;
          this[newClassFieldName] = {
            className: newClassFieldName as string,
            label: newClassFieldLabel as string,
            value: newClassFieldValue as string
          };
          this.orderContent.push({
            className: this[newClassFieldName as any].className,
            label: this[newClassFieldName as any].label,
            value: this[newClassFieldName as any].value
          });      // hash for protection files from direct downloads in loop.
        }
      }
      if (this.userService.user.userType === (enumUserTypes.employee || enumUserTypes.manager || enumUserTypes.root)) {
        this.orderService.markAsReadOrder(this.orderService.order._id).subscribe(notification => {
          // Refresh notification
          this.notificationService.getNotification().subscribe(notification => {
            this.notificationService.notification = notification;
          });
        });
      }
    });

    this.orderEdit = false;
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

      file.file['originalName'] = file.file.name;
      let fileExtensions = file.file.name.slice((file.file.name.lastIndexOf('.') - 1 >>> 0) + 2);
      // hash for protection files from direct downloads in loop and solving cyrillic naming problems
      file.file.name = moment().format('YYYY-MM-DD-HH-MM-SS_') + Math.random().toString(36).substring(2, 12)
        + Math.random().toString(36).substring(2, 12) + '.' + fileExtensions;
    };
    // this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //   console.log('ImageUpload:uploaded:', item, status, response);
    //   if (status === 201) {
    //     for (let itemF in this.uploader) {
    //       if (this.uploader.hasOwnProperty(itemF)) {
    //       }
    //     }
    //   }
    // };
  }

  editOrder() {
    this.orderEdit = true;
  }

  closeOrder(orderId: string) {
    this.orderService.order.isOrderOpen = false;
    this.orderService.closeOrder(orderId).subscribe(response => {
      this.orderService.order.orderContent = response.orderContent;
      this.router.navigate(['account/orders-list']);
    });
  }

  cancelEditOrder() {
    // Canceled function in message service on cancelOrderAndCreateMessage name
    this.deletingOrders = [];
    this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.orderService.getOrder(params.get('orderId'))).subscribe(response => {

      this.orderService.order = response;
      // This code find className fields in orderContent for dynamic creating fields in class for using it in
      // ngModel and sending new order after editing. It code need for universal creating fields for different
      // order types with different fields.
      for (let item in this.orderService.order.orderContent) {
        if (this.orderService.order.orderContent.hasOwnProperty(item)) {
          let newClassFieldName: string = this.orderService.order.orderContent[item].className;
          let newClassFieldLabel: string = this.orderService.order.orderContent[item].label;
          let newClassFieldValue: string = this.orderService.order.orderContent[item].value;
          this[newClassFieldName] = {
            className: newClassFieldName as string,
            label: newClassFieldLabel as string,
            value: newClassFieldValue as string
          };
          this.orderContent.push({
            className: this[newClassFieldName as any].className,
            label: this[newClassFieldName as any].label,
            value: this[newClassFieldName as any].value
          });      // hash for protection files from direct downloads in loop.
        }
      }
    });
    this.orderEdit = false;
  }

  deleteFile(fileUrl: string) {
    // Create array for pending deleting files after user's click saving changes order.
    this.deletingOrders.push(fileUrl);
    // Only delete files from UI. Not real deleting. Real delete from server in submit() function.
    for (let i = 0; i < this.orderService.order.orderContent.length; i += 1) {
      if (this.orderService.order.orderContent[i].className === 'orderContentFiles') {
        for (let f = 0; f < this.orderService.order.orderContent[i].value.length; f += 1) {
          if (this.orderService.order.orderContent[i].value[f].fileUrl === fileUrl) {
            this.orderService.order.orderContent[i].value.splice(f, 1);
          }
        }
      }
    }
  }

  submit(orderId: string) {
    // TODO: Have bug, when user change several times files. Download it in edit mode, save and after enter in edit mode again -
    // download files be in queue. Don't delete queue directly it not working like this.
    if (this.deletingOrders.length > 0) {
      for (let fileForDeleting = 0; fileForDeleting < this.deletingOrders.length; fileForDeleting += 1) {
        // Find deleted file in order and delete in from array.
        for (let i = 0; i < this.orderService.order.orderContent.length; i += 1) {
          if (this.orderService.order.orderContent[i].className === 'orderContentFiles') {
            for (let f = 0; f < this.orderService.order.orderContent[i].value.length; f += 1) {
              if (this.orderService.order.orderContent[i].value[f].fileUrl === this.deletingOrders[fileForDeleting]) {
                this.orderService.order.orderContent[i].value.splice(f, 1);
                this.orderService.deleteFileFromServer(this.deletingOrders[fileForDeleting], this.orderService.order._id,
                  this.orderService.order.orderContent).subscribe();
              }
            }
          }
        }
      }
      this.deletingOrders = [];
    }
    this.orderContent = [];
    for (let item in this) {
      if (this.hasOwnProperty(item) &&
        item.substring(0, 17) === 'orderContentField') {
        this.orderContent.push({
          // as any in items need for solve first run compiler warning.
          // https://stackoverflow.com/
          // questions/49318768/angular-5-have-error-on-first-compile-error-ts2339-but-after-success-compila/49319343#49319343
          className: this[item as any].className,
          label: this[item as any].label,
          value: this[item as any].value
        });
      }
    }
    const orderFiles = [];
    for (let item = 0; item < this.uploader.queue.length; item += 1) {
      orderFiles.push({fileUrl: this.uploader.queue[item].file.name, fileName: this.uploader.queue[item].file['originalName']});
    }
    for (let i = 0; i < this.orderService.order.orderContent.length; i += 1) {
      if (this.orderService.order.orderContent[i].className === 'orderContentFiles') {
        this.orderService.order.orderContent[i].value = [...this.orderService.order.orderContent[i].value, ...orderFiles];
        this.orderContent = [...this.orderContent, ...this.orderService.order.orderContent[i]];
      }
    }
    this.orderService.order.orderDataUpdate = new Date();
    this.orderService.updateOrder(orderId, this.orderContent).subscribe(response => {
      this.orderService.order.orderContent = response.orderContent;
      // Refresh notification
      this.notificationService.getNotification().subscribe(notification => {
        this.notificationService.notification = notification;
      });
    });
    this.orderEdit = false;
  }


}
