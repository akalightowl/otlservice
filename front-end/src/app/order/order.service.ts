import {Injectable} from '@angular/core';
import {Order} from './order.model';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user/user.service';
import {Observable} from 'rxjs/Observable';
import {apiRoutes} from '../app.config';
import {Message} from '../message/message.model';

@Injectable()
export class OrderService {
  order: Order = new Order();
  orderList: Array<Order> = [];

  constructor(private router: Router,
              private httpClient: HttpClient,
              public userService: UserService) {
  }


  createOrder(userId: string, orderName: string, orderCost: number, orderDeadLine: string, orderContent: any): Observable<Order> {
    return this.httpClient.post<Order>(apiRoutes.orders,
      {
        userId: userId,
        orderName: orderName,
        orderCost: orderCost,
        isOrderOpen: true,
        isOrderCancel: this.order.isOrderCancel,
        isOrderPaidOf: this.order.isOrderPaidOf,
        orderWorkStatus: this.order.orderWorkStatus,
        orderPaymentStatus: this.order.orderPaymentStatus,
        orderDeadLine: orderDeadLine,
        orderDataOpened: this.order.orderDataOpened,
        orderDataUpdate: this.order.orderDataUpdate,
        orderDataClosed: this.order.orderDataClosed,
        orderDataPayment: this.order.orderDataPayment,
        orderNotViewed: true,
        orderMessages: this.order.orderMessages,
        haveNotReadMessages: this.order.haveNotReadMessages,
        orderContent: orderContent
      });
  }

  updateOrder(orderId: string, orderContent: any): Observable<Order> {

    return this.httpClient.put<Order>(`${apiRoutes.orders}/${orderId}`,
      {
        orderDataUpdate: this.order.orderDataUpdate,
        orderContent: orderContent
      });
  }

  markAsReadOrder(orderId: string): Observable<Order> {
    return this.httpClient.put<Order>(`${apiRoutes.orders}/${orderId}/mark-as-read`, {});
  }

  // Not use delete http, because we don't delete any order. Only cancel and hide.
  cancelOrder(orderId: string): Observable<Order> {
    return this.httpClient.put<Order>(`${apiRoutes.orders}/${orderId}/cancel`,
      {
        isOrderOpen: this.order.isOrderOpen,
        isOrderCancel: this.order.isOrderCancel
      });
  }

    // Not use delete http, because we don't delete any order. Only close and hide.
  closeOrder(orderId: string): Observable<Order> {
    return this.httpClient.put<Order>(`${apiRoutes.orders}/${orderId}/close`,
      {
        isOrderOpen: this.order.isOrderOpen,
      });
  }

  getUserOpenedOrders(): Observable<Array<Order>> {
    return this.httpClient.post<Array<Order>>(apiRoutes.userOpenedOrders,
      {_id: this.userService.user._id});
  }

  getUserClosedOrders(): Observable<Array<Order>> {
    return this.httpClient.post<Array<Order>>(apiRoutes.userClosedOrders,
      {_id: this.userService.user._id});
  }

  getAllOpenedOrders(): Observable<Array<Order>> {
    return this.httpClient.get<Array<Order>>(apiRoutes.openedOrders, {});
  }

  getAllClosedOrders(): Observable<Array<Order>> {
    return this.httpClient.get<Array<Order>>(apiRoutes.closedOrders, {});
  }

  getAllCanceledOrders(): Observable<Array<Order>> {
    return this.httpClient.get<Array<Order>>(apiRoutes.canceledOrders, {});
  }

  getOrder(orderId: string): Observable<Order> {
    return this.httpClient.get<Order>(`${apiRoutes.orders}/${orderId}`);
  }

  deleteFileFromServer(fileName: string, orderId: string, orderContent: any): Observable<any> {
    return this.httpClient.put<any>(`${apiRoutes.fileUpload}/${fileName}`,
      {
        orderId: orderId,
        orderContent: orderContent
      });
  }


  resetOrder(): void {
    this.order._id = null;
    this.order.orderName = null;
    this.order.orderCost = null;
    this.order.isOrderOpen = true;
    this.order.isOrderCancel = false;
    this.order.isOrderPaidOf = false;
    this.order.orderWorkStatus = 'Заказ выполняется';
    this.order.orderPaymentStatus = 'Не оплачено';
    this.order.orderDeadLine = '3–4 дня';
    this.order.orderDataOpened = new Date();
    this.order.orderDataUpdate = null;
    this.order.orderDataClosed = null;
    this.order.orderDataPayment = null;
    this.order.orderNotViewed = true;
    this.order.orderMessages = [{}];
    this.order.haveNotReadMessages = true;
    this.order.orderContent = null;
  }
}
