export class Order {
  constructor(
    public _id: string = null,
    public userId: string = null,
    public orderName: string = '',
    public orderCost: number = null,
    public isOrderOpen: boolean = true,
    public isOrderCancel: boolean = false,
    public isOrderPaidOf: boolean = false,
    public orderWorkStatus: string = 'Заказ выполняется',
    public orderPaymentStatus: string = 'Не оплачено',
    public orderDeadLine: string = '3–4 дня',
    public orderDataOpened: Date = new Date(),
    public orderDataUpdate: Date = null,
    public orderDataClosed: Date = null,
    public orderDataPayment: Date = null,
    public orderNotViewed: boolean = true,
    public orderMessages: Array<any> = [{}],
    public haveNotReadMessages: boolean = true,
    public orderContent: Array<any> = [{}]
  ) {
  }
}
