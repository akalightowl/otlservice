import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {UserService} from '../user/user.service';
import {AuthenticationService} from '../authentication/authentication.service';


@Injectable()
export class GuardService {

  constructor(public userService: UserService,
              public router: Router,
              public authenticationService: AuthenticationService) {
  }

  canActivate(): boolean {
    if (this.userService.user.isUserAuthenticated === false) {
      this.authenticationService.roleError = true;
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
