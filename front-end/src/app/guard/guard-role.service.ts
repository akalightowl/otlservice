import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {UserService} from '../user/user.service';
import * as decode from 'jwt-decode';
import {AuthenticationService} from '../authentication/authentication.service';

@Injectable()
export class GuardRoleService implements CanActivate {

  constructor(public userService: UserService, public router: Router, public authenticationService: AuthenticationService) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole;
    const token = localStorage.getItem('token');
    const userType = decode(token).userType;
    if (
      this.userService.user.isUserAuthenticated === false ||
      expectedRole.includes(userType) === false
    ) {
      this.authenticationService.roleError = true;
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
