import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {enumUserTypes, User} from '../user/user.model';
import {Observable} from 'rxjs/Observable';
import {apiRoutes} from '../app.config';
import 'rxjs/add/operator/map';
import {UserService} from '../user/user.service';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication/authentication.service';
import * as moment from 'moment';
import {OrderService} from '../order/order.service';

@Injectable()
export class RegistrationService {
  haveError = false;
  errorText: string;
  registrationAfterOrder = false;

  constructor(private router: Router,
              private httpClient: HttpClient,
              public userService: UserService,
              public orderService: OrderService,
              public authenticationService: AuthenticationService) {
  }

  createUser(email: string, password: string, type: string, userName?: string, userDepartment?: string): Observable<User> {
    if (this.registrationAfterOrder === true) {
      return this.httpClient.post<User>(apiRoutes.registrationAfterOrder, {
        userEmail: email,
        userPassword: password,
        userType: type,
        orderName: this.orderService.order.orderName,
        orderCost: this.orderService.order.orderCost,
        isOrderOpen: true,
        isOrderCancel: this.orderService.order.isOrderCancel,
        isOrderPaidOf: this.orderService.order.isOrderPaidOf,
        orderWorkStatus: this.orderService.order.orderWorkStatus,
        orderPaymentStatus: this.orderService.order.orderPaymentStatus,
        orderDeadLine: this.orderService.order.orderDeadLine,
        orderDataOpened: this.orderService.order.orderDataOpened,
        orderDataUpdate: this.orderService.order.orderDataUpdate,
        orderDataClosed: this.orderService.order.orderDataClosed,
        orderDataPayment: this.orderService.order.orderDataPayment,
        orderNotViewed: true,
        orderMessages: this.orderService.order.orderMessages,
        haveNotReadMessages: this.orderService.order.haveNotReadMessages,
        orderContent: this.orderService.order.orderContent
      });
    } else {
      return this.httpClient.post<User>(apiRoutes.registration, {
        userEmail: email,
        userPassword: password,
        userType: type,
        userName: userName,
        userDepartment: userDepartment,
      });
    }
  }

  registrationUser(email: string, password: string, type: string, userName?: string, userDepartment?: string) {
    this.createUser(email, password, type, userName, userDepartment).subscribe(data => {
      if (data.userToken.length > 0) {
        this.userService.user.userEmail = data.userEmail;
        this.userService.user.userToken = data.userToken;
        this.userService.user.userName = data.userName;
        this.userService.user.userDepartment = data.userDepartment;
        this.userService.user._id = data._id;
        this.userService.user.userType = data.userType;
        this.userService.user.isUserAuthenticated = true;
        this.userService.user.userOrders = data.userOrders;
        this.userService.user.userTasksCreator = data.userTasksCreator;
        this.userService.user.userTasksExecutor = data.userTasksExecutor;

        const expiresAt = moment().add(data.expiresInMinutesJWT, 'day');
        localStorage.setItem('token', JSON.stringify(data.userToken).slice(1, -1));
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
        this.orderService.resetOrder();
        this.registrationAfterOrder = false;
        this.haveError = false;
        switch (data.userType) {
          case enumUserTypes.user:
            this.router.navigate(['account/orders-list']);
            break;
          default:
            this.router.navigate(['/']);
        }
      }
    }, error => {
      if (error.status === 409) {
        this.errorText = error.error.message;
        this.errorText = error.error.message;
        this.haveError = true;
      } else {
        console.log(error);
      }
    });
  }
}
