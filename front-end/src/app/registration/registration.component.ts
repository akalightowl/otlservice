import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, FormArray, NgForm} from '@angular/forms';
import {RegistrationService} from './registration.service';
import {UserService} from '../user/user.service';
import {enumUserTypes} from '../user/user.model';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  animations: [
    trigger('showBlock', [
      state('hide', style({
        opacity: '0',
        transform: 'scale(.96)',
        display: 'none'
      })),
      state('show', style({
        opacity: '1',
        display: 'block'
      })),
      transition('hide => show', animate('210ms ease-in')),
      transition('show => hide', animate('160ms ease-out'))
    ])
  ]
})
export class RegistrationComponent implements OnInit {
  userEmail: string;
  userPassword: string;
  userPasswordConfirm: string;
  userType: enumUserTypes;
  userName: string;
  userDepartment: string;
  userTypesList: Array<String>; // Array for use enum enumUserTypes in template with ngFor.
  userTypes = enumUserTypes;
  showPopUp = false;
  popUpMessagesStates = {
    errorEmail: true as boolean,
    errorPassword: true as boolean,
    passwordConfirmError: true as boolean,
    errorServer: true as boolean,
  };


  constructor(public registrationService: RegistrationService, public userService: UserService) {
  }

  ngOnInit() {
    // Create Array from enum for template ngFor.
    this.userTypesList = [];
    for (let type in this.userTypes) {
      if (this.userTypes.hasOwnProperty(type)) {
        this.userTypesList.push(type);
      }
    }
  }

  onSubmit() {
    if (this.userService.user.userType === this.userTypes.root) {
      this.registrationService.registrationUser(this.userEmail, this.userPassword, this.userType, this.userName, this.userDepartment);
    } else {
      this.registrationService.registrationUser(this.userEmail, this.userPassword, this.userTypes.user);
    }
  }

  showPopUps() {
    this.showPopUp = true;
    // Show all errors pop-ups
    for (let errorState in this.popUpMessagesStates) {
      if (this.popUpMessagesStates.hasOwnProperty(errorState)) {
        this.popUpMessagesStates[errorState] = true;
      }
    }
  }

  closePopUp(popUpMessageName: string) {
    this.popUpMessagesStates[popUpMessageName] = false;
    if (popUpMessageName === 'errorServer') {
      this.registrationService.haveError = false;
      this.registrationService.errorText = '';
    }
  }

}
