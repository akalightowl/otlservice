import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page3ndflComponent } from './page--3ndfl.component';

describe('Page3ndflComponent', () => {
  let component: Page3ndflComponent;
  let fixture: ComponentFixture<Page3ndflComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page3ndflComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page3ndflComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
