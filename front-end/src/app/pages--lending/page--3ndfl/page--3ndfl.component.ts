import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {NgForm} from '@angular/forms';
import {OrderService} from '../../order/order.service';
import {UserService} from '../../user/user.service';
import {NotificationService} from '../../notification/notification.service';
import {FileUploader} from 'ng2-file-upload';
import {apiRoutes} from '../../app.config';
import * as moment from 'moment';
import {RegistrationService} from '../../registration/registration.service';
import {AuthenticationService} from '../../authentication/authentication.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-page--3ndfl',
  templateUrl: './page--3ndfl.component.html',
  styleUrls: ['./page--3ndfl.component.scss'],
  animations: [
    trigger('showBlock', [
      state('hide', style({
        opacity: '0',
        transform: 'scale(.96)',
        display: 'none'
      })),
      state('show', style({
        opacity: '1',
        display: 'block'
      })),
      transition('hide => show', animate('210ms ease-in')),
      transition('show => hide', animate('160ms ease-out'))
    ]),
    trigger('filter', [
      state('hide', style({
        backgroundColor: 'rgba(255, 255, 255, .88)',
      })),
      state('show', style({
        backgroundColor: 'rgba(255, 255, 255, 0)',
      })),
      transition('hide => show', animate('210ms ease-in')),
      transition('show => hide', animate('160ms ease-out'))
    ])
  ]
})
export class Page3ndflComponent implements OnInit {
  // User object with value and label for order.component dynamic generate content.
  // User orderContentField in name for protect from occasional reassign default class fields in cycle.
  // It uses in generate dynamic editable form in order.component.
  ndfl3FormName: string;
  ndfl3ContentName = {
    className: 'orderContentFieldName' as string,
    label: 'Имя' as string,
  };
  ndfl3FormSurname: string;
  ndfl3ContentSurname = {
    className: 'orderContentFieldSurname' as string,
    label: 'Фамилия' as string,
  };
  ndfl3FormPatronymic: string;
  ndfl3ContentPatronymic = {
    className: 'orderContentFieldPatronymic' as string,
    label: 'Отчество' as string,
  };
  ndfl3FormPhone: string;
  ndfl3ContentPhone = {
    className: 'orderContentFieldPhone' as string,
    label: 'Tелефон' as string,
  };
  ndfl3ContentFiles = {
    className: 'orderContentFiles' as string,
    label: 'Прикрепленные файлы'
  };
  stateAllCards = 'show';
  stateTablePrice = 'show';
  statesCards = {
    buyHouse: 'hide' as string,
    hypothec: 'hide' as string,
    iis: 'hide' as string,
    medical: 'hide' as string,
    insurance: 'hide' as string,
    learning: 'hide' as string
  };
  statesForms = {
    stepFirst: 'hide' as string,
    stepSecond: 'hide' as string,
    authenticationForm: 'hide' as string,
    registrationForm: 'show' as string
  };
  orderRateOne = {
    price: 2000 as number,
    name: 'Налоговый вычет 3 НДФЛ' as string,
    deadLine: '3–4 дня' as string
  };
  orderRateTwo = {
    price: 4000 as number,
    name: 'Налоговый вычет 3 НДФЛ' as string,
    deadLine: '3–4 дня' as string
  };
  uploader: FileUploader = new FileUploader({url: apiRoutes.fileUpload, autoUpload: true});
  hasBaseDropZoneOver = false;
  showPopUp = false;
  popUpMessagesStates = {
    infoFiles: true as boolean,
    errorName: false as boolean,
    errorSurname: false as boolean,
    errorPatronymic: false as boolean,
    errorPhone: false as boolean,
    errorServer: true as boolean,
  };

  constructor(private router: Router,
              public orderService: OrderService,
              public userService: UserService,
              public notificationService: NotificationService,
              public registrationService: RegistrationService,
              public authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;

      file.file['originalName'] = file.file.name;
      let fileExtensions = file.file.name.slice((file.file.name.lastIndexOf('.') - 1 >>> 0) + 2);
      // hash for protection files from direct downloads in loop and solving cyrillic naming problems
      file.file.name = moment().format('YYYY-MM-DD-HH-MM-SS_') + Math.random().toString(36).substring(2, 12)
        + Math.random().toString(36).substring(2, 12) + '.' + fileExtensions;
      this.showPopUp = true;
    };
    // this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //   console.log('ImageUpload:uploaded:', item, status, response);
    //   if (status === 201) {
    //     for (let itemF in this.uploader) {
    //       if (this.uploader.hasOwnProperty(itemF)) {
    //       }
    //     }
    //   }
    // };
  }

  submit() {
    const orderFiles = [];
    for (let item = 0; item < this.uploader.queue.length; item += 1) {
      orderFiles.push({fileUrl: this.uploader.queue[item].file.name, fileName: this.uploader.queue[item].file['originalName']});
    }
    const orderContent = [
      {
        className: this.ndfl3ContentName.className as string,
        label: this.ndfl3ContentName.label as string,
        value: this.ndfl3FormName as string
      },
      {
        className: this.ndfl3ContentSurname.className as string,
        label: this.ndfl3ContentSurname.label as string,
        value: this.ndfl3FormSurname as string
      },
      {
        className: this.ndfl3ContentPatronymic.className as string,
        label: this.ndfl3ContentPatronymic.label as string,
        value: this.ndfl3FormPatronymic as string
      },
      {
        className: this.ndfl3ContentPhone.className as string,
        label: this.ndfl3ContentPhone.label as string,
        value: this.ndfl3FormPhone as string
      },
      {
        className: this.ndfl3ContentFiles.className as string,
        label: this.ndfl3ContentFiles.label as string,
        value: orderFiles
      }
    ];
    // TODO: Don't forger clean order after sending
    this.orderService.createOrder(
      this.userService.user._id,
      this.orderService.order.orderName,
      this.orderService.order.orderCost,
      this.orderService.order.orderDeadLine,
      orderContent).subscribe(response => {
      // Refresh notification
      this.notificationService.getNotification().subscribe(notification => {
        this.notificationService.notification = notification;
        this.router.navigate(['account/orders-list']);
      });
    });
  }

  showBlock(cardName: string) {
    this.statesCards[cardName] = 'show';
    this.stateAllCards = 'hide';
  }

  backToAllCards() {
    for (const stateCard in this.statesCards) {
      if (this.statesCards.hasOwnProperty(stateCard)) {
        this.statesCards[stateCard] = 'hide';
      }
    }
    this.stateAllCards = 'show';
  }

  showForm(formName: string) {
    for (const stateForm in this.statesForms) {
      if (this.statesForms.hasOwnProperty(stateForm)) {
        if (stateForm !== 'authenticationForm' && stateForm !== 'registrationForm') {
          this.statesForms[stateForm] = 'hide';
        }
      }
    }
    this.statesForms[formName] = 'show';
    this.stateTablePrice = 'hide';
    // Code for creation tmp order for adding it after registration
    if (formName === 'stepSecond') {
      this.registrationService.registrationAfterOrder = true;
      this.authenticationService.authenticationAfterOrder = true;
      const orderFiles = [];
      for (let item = 0; item < this.uploader.queue.length; item += 1) {
        orderFiles.push({fileUrl: this.uploader.queue[item].file.name, fileName: this.uploader.queue[item].file['originalName']});
      }
      this.orderService.order.orderContent = [
        {
          className: this.ndfl3ContentName.className as string,
          label: this.ndfl3ContentName.label as string,
          value: this.ndfl3FormName as string
        },
        {
          className: this.ndfl3ContentSurname.className as string,
          label: this.ndfl3ContentSurname.label as string,
          value: this.ndfl3FormSurname as string
        },
        {
          className: this.ndfl3ContentPatronymic.className as string,
          label: this.ndfl3ContentPatronymic.label as string,
          value: this.ndfl3FormPatronymic as string
        },
        {
          className: this.ndfl3ContentPhone.className as string,
          label: this.ndfl3ContentPhone.label as string,
          value: this.ndfl3FormPhone as string
        },
        {
          className: this.ndfl3ContentFiles.className as string,
          label: this.ndfl3ContentFiles.label as string,
          value: orderFiles
        }
      ];
    }
  }

  switchTab(tabName: string) {
    if (tabName === 'authenticationForm') {
      this.statesForms['registrationForm'] = 'hide';
      this.statesForms['authenticationForm'] = 'show';
    }
    if (tabName === 'registrationForm') {
      this.statesForms['authenticationForm'] = 'hide';
      this.statesForms['registrationForm'] = 'show';
    }
  }

  backToTablePrice() {
    for (const stateForm in this.statesForms) {
      if (this.statesForms.hasOwnProperty(stateForm)) {
        if (stateForm !== 'authenticationForm' && stateForm !== 'registrationForm') {
          this.statesForms[stateForm] = 'hide';
        }
      }
    }
    this.stateTablePrice = 'show';
  }

  selectOrderRate(price: number, name: string, deadLine: string) {
    this.orderService.order.orderCost = price;
    this.orderService.order.orderName = name;
    this.orderService.order.orderDeadLine = deadLine;
    this.showForm('stepFirst');
  }

  showPopUps() {
    console.log(this.popUpMessagesStates);
    this.showPopUp = true;
    // Show all errors pop-ups
    for (let errorState in this.popUpMessagesStates) {
      if (this.popUpMessagesStates.hasOwnProperty(errorState)) {
        this.popUpMessagesStates[errorState] = true;
      }
    }
  }


  closePopUp(popUpMessageName: string) {
    console.log(this.popUpMessagesStates);
    this.popUpMessagesStates[popUpMessageName] = false;
  }

  fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

}
