import {Component, Input, OnInit} from '@angular/core';
import {enumUserTypes, User} from '../user/user.model';
import {Notification} from '../notification/notification.model';

@Component({
  selector: 'app-menu--account',
  templateUrl: './menu--account.component.html',
  styleUrls: ['./menu--account.component.scss']
})
export class MenuAccountComponent implements OnInit {
  @Input() user: User;
  @Input() notification: Notification;
  userTypes = enumUserTypes;
  constructor() { }

  ngOnInit() {

  }

}
