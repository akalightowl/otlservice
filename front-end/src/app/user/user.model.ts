export enum enumUserTypes {
  guest = 'guest',
  user = 'user',
  employee = 'employee',
  manager = 'manager',
  root = 'root'
}

export class User {
  constructor(
    public _id: string = null,
    public expiresInMinutesJWT: number = 1440,
    public userEmail: string = null,
    public userType: enumUserTypes = enumUserTypes.guest,
    public userName: string = null,
    public userDepartment: string = null,
    public userToken: string = null,
    public isUserAuthenticated: boolean = false,
    public userOrders: Array<any> = null,
    public userTasksCreator: Array<any> = null,
    public userTasksExecutor: Array<any> = null
  ) {
  }
}
