import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthenticationComponent} from './authentication/authentication.component';
import {AuthenticationInterceptor} from './authentication/authentication.interceptor';
import {AuthenticationService} from './authentication/authentication.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HelpComponent} from './account/help/help.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MenuAccountComponent} from './menu--account/menu--account.component';
import {MenuFooterComponent} from './menu--footer/menu--footer.component';
import {MenuMainComponent} from './menu--main/menu--main.component';
import {MessageComponent} from './message/message.component';
import {MessageService} from './message/message.service';
import {NgModule} from '@angular/core';
import {NotificationComponent} from './notification/notification.component';
import {NotificationService} from './notification/notification.service';
import {OrderComponent} from './order/order.component';
import {OrderService} from './order/order.service';
import {OrdersListComponent} from './account/orders-list/orders-list.component';
import {Page3ndflComponent} from './pages--lending/page--3ndfl/page--3ndfl.component';
import {PageAboutComponent} from './pages--content/page--about/page--about.component';
import {PageAuthenticationComponent} from './pages--content/page--authentication/page--authentication.component';
import {PageBusinessComponent} from './pages--content/page--business/page--business.component';
import {PageHomeComponent} from './pages--content/page--home/page--home.component';
import {PagePersonalComponent} from './pages--content/page--personal/page--personal.component';
import {PageRegistrationComponent} from './pages--content/page--registration/page--registration.component';
import {PageStartupComponent} from './pages--content/page--startup/page--startup.component';
import {PaymentsListComponent} from './account/payments-list/payments-list.component';
import {RegistrationComponent} from './registration/registration.component';
import {RegistrationService} from './registration/registration.service';
import {SettingsComponent} from './account/settings/settings.component';
import {TaskComponent} from './task/task.component';
import {TaskService} from './task/task.service';
import {UserService} from './user/user.service';
import {GuardService} from './guard/guard.service';
import {GuardRoleService} from './guard/guard-role.service';
import {FileUploadModule} from 'ng2-file-upload';
import {TaskListComponent} from './account/task-list/task-list.component';
import {ClientsListComponent} from './account/clients-list/clients-list.component';
import {ClientsListService} from './account/clients-list/clients-list.service';
import {EmployeesListComponent} from './account/employees-list/employees-list.component';
import {EmployeesListService} from './account/employees-list/employees-list.service';

@NgModule({
  declarations: [
    AppComponent,
    MenuMainComponent,
    MenuAccountComponent,
    AuthenticationComponent,
    PageHomeComponent,
    PageAboutComponent,
    PagePersonalComponent,
    PageStartupComponent,
    PageBusinessComponent,
    RegistrationComponent,
    MenuFooterComponent,
    Page3ndflComponent,
    PageAuthenticationComponent,
    PageRegistrationComponent,
    OrdersListComponent,
    PaymentsListComponent,
    SettingsComponent,
    HelpComponent,
    OrderComponent,
    MessageComponent,
    TaskComponent,
    NotificationComponent,
    TaskListComponent,
    EmployeesListComponent,
    ClientsListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FileUploadModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true},
    UserService,
    AuthenticationService,
    RegistrationService,
    OrderService,
    MessageService,
    TaskService,
    NotificationService,
    GuardService,
    GuardRoleService,
    EmployeesListService,
    ClientsListService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
