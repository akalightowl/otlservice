import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {TaskService} from './task.service';
import {OrderService} from '../order/order.service';
import {UserService} from '../user/user.service';
import {enumUserTypes} from '../user/user.model';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  taskName: string;
  userEmail: string;
  listOfEmployees: Array<String>;
  taskExecutorId: string;
  taskPriority: string;
  taskPrioritiesList = ['Низкий', 'Обычный', 'Высокий', 'Срочный! Проблемы с клиентом!'];
  taskStatus: string;
  taskStatusesList = ['Не просмотрено', 'Просмотрено', 'Отменено в связи с ошибкой в задаче', 'Принято в работу', 'Выполнено'];
  // TODO: Create Data with datapicker
  taskDeadline: string;
  taskContent: string;
  taskState = 'createTask';

  constructor(private route: ActivatedRoute,
              private router: Router,
              public orderService: OrderService,
              public userService: UserService,
              public taskService: TaskService) {
  }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.taskService.getTask(params.get('taskId'))).subscribe(response => {
      if (response.status === 200) {
        this.taskService.task = response.task;
        this.taskState = 'viewTask';
      } else {
        this.taskState = 'createTask';
      }
    });
    this.taskService.getListOfEmployees().subscribe(response => {
      this.listOfEmployees = response;
    });
    console.log(this.taskState, this.taskService.task);
  }

  createTask() {
    this.taskService.createTask(this.taskName, this.taskExecutorId, this.taskPriority,
      this.taskDeadline, this.taskContent).subscribe(response => {
        this.router.navigate(['account/tasks-list']);
    });
  }

  cancelTask() {
    this.taskStatus = 'Отменено в связи с ошибкой в задаче';
  }

  againTask() {
    this.taskStatus = 'Принято в работу';
  }

    startWorking() {
    this.taskStatus = 'Принято в работу';
  }

  finishTask() {
    this.taskStatus = 'Выполнено';
  }

}
