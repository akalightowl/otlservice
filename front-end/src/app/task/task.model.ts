export class Task {
  constructor(
    public _id: string = null,
    public taskName: string = null,
    public taskCreatorId: string = null,
    public taskCreatorName: string = null,
    public taskExecutorId: string = null,
    public taskTargetUserId: string = null,
    public taskTargetOrderId: string = null,
    public taskTargetOrderName: string = null,
    public taskStatus: string = 'Обычный',
    public taskPriority: string = 'Не просмотрено',
    public isTaskOpen: boolean = true,
    public taskDataOpened: Date = new Date(),
    public taskDeadline: string = null,
    public taskDataUpdate: Date = null,
    public taskDataClosed: Date = null,
    public taskContent: string = null
  ) {
  }
}
