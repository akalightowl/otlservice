import {Injectable} from '@angular/core';
import {Task} from './task.model';
import {UserService} from '../user/user.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {OrderService} from '../order/order.service';
import {apiRoutes} from '../app.config';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class TaskService {
  task: Task = new Task();

  constructor(private router: Router,
              private httpClient: HttpClient,
              public userService: UserService,
              public orderService: OrderService) {
  }

  createTask(
    taskName: string,
    taskExecutorId: string,
    taskPriority: any,
    taskDeadline: string,
    taskContent: any,
    taskStatus = 'Не просмотрено'
  ): Observable<Task> {
    return this.httpClient.post<Task>(apiRoutes.tasks,
      {
        taskName: taskName,
        taskCreatorId: this.userService.user._id,
        taskCreatorName: this.userService.user.userName,
        taskExecutorId: taskExecutorId,
        taskTargetUserId: this.orderService.order.userId,
        taskTargetOrderId: this.orderService.order._id,
        taskTargetOrderName: this.orderService.order.orderName,
        taskStatus: taskStatus,
        taskPriority: taskPriority,
        isTaskOpen: this.task.isTaskOpen,
        taskDataOpened: this.task.taskDataOpened,
        taskDeadline: taskDeadline,
        taskDataUpdate: this.task.taskDataUpdate,
        taskDataClosed: this.task.taskDataClosed,
        taskContent: taskContent
      });
  }

  getListOfEmployees(): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.employees}`);
  }

  getTasksUserExecutor(userId: string): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.tasks}/${userId}/executor`);
  }

  getTasksUserCreator(userId: string): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.tasks}/${userId}/Creator`);
  }

  getTask(taskId: string): Observable<any> {
    return this.httpClient.get<any>(`${apiRoutes.tasks}/${taskId}`);
  }
}
