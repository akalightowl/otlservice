import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {HttpHeaders} from '@angular/common/http';
import {UserService} from '../user/user.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  animations: [
    trigger('showBlock', [
      state('hide', style({
        opacity: '0',
        transform: 'scale(.96)',
        display: 'none'
      })),
      state('show', style({
        opacity: '1',
        display: 'block'
      })),
      transition('hide => show', animate('210ms ease-in')),
      transition('show => hide', animate('160ms ease-out'))
    ])
  ]
})
export class AuthenticationComponent implements OnInit {
  userEmail: string;
  userPassword: string;
  showPopUp = false;
  popUpMessagesStates = {
    errorEmail: true as boolean,
    errorPassword: true as boolean,
    errorServer: true as boolean,
  };

  constructor(public authenticationService: AuthenticationService, public userService: UserService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authenticationService.authenticate(this.userEmail, this.userPassword);
  }

  showPopUps() {
    console.log(this.popUpMessagesStates);
    this.showPopUp = true;
    // Show all errors pop-ups
    for (let errorState in this.popUpMessagesStates) {
      if (this.popUpMessagesStates.hasOwnProperty(errorState)) {
        this.popUpMessagesStates[errorState] = true;
      }
    }
  }

  closePopUp(popUpMessageName: string) {
    console.log(this.popUpMessagesStates);
    this.popUpMessagesStates[popUpMessageName] = false;
    if (popUpMessageName === 'errorServer') {
      this.authenticationService.haveError = false;
      this.authenticationService.errorText = '';
    }
    if (popUpMessageName === 'roleError') {
      this.authenticationService.roleError = false;
    }
  }

}
