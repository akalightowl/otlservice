import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {enumUserTypes, User} from '../user/user.model';
import {Observable} from 'rxjs/Observable';
import {apiRoutes} from '../app.config';
import 'rxjs/add/operator/map';
import {UserService} from '../user/user.service';
import {Router} from '@angular/router';
import * as moment from 'moment';
import * as decode from 'jwt-decode';
import {OrderService} from '../order/order.service';

@Injectable()
export class AuthenticationService {
  haveError = false;
  roleError = false;
  errorText: string;
  token: string;
  authenticationAfterOrder = false;

  constructor(private router: Router,
              private httpClient: HttpClient,
              public userService: UserService,
              public orderService: OrderService) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  getToken(email: string, password: string): Observable<User> {
    return this.httpClient.post<User>(apiRoutes.authentication, {userEmail: email, userPassword: password});
  }

  getTokenAndCreateOrder(email: string, password: string): Observable<User> {
    return this.httpClient.post<User>(apiRoutes.authenticationAfterOrder,
      {
        userEmail: email,
        userPassword: password,
        orderName: this.orderService.order.orderName,
        orderCost: this.orderService.order.orderCost,
        isOrderOpen: true,
        isOrderCancel: false,
        isOrderPaidOf: false,
        orderWorkStatus: this.orderService.order.orderWorkStatus,
        orderPaymentStatus: this.orderService.order.orderPaymentStatus,
        orderDeadLine: this.orderService.order.orderDeadLine,
        orderDataOpened: this.orderService.order.orderDataOpened,
        orderDataUpdate: this.orderService.order.orderDataUpdate,
        orderDataClosed: this.orderService.order.orderDataClosed,
        orderDataPayment: this.orderService.order.orderDataPayment,
        orderNotViewed: true,
        orderMessages: this.orderService.order.orderMessages,
        haveNotReadMessages: this.orderService.order.haveNotReadMessages,
        orderContent: this.orderService.order.orderContent
      });
  }

  authenticate(email: string, password: string) {
    console.log(this.orderService.order);
    if (this.authenticationAfterOrder === true) {
      this.getTokenAndCreateOrder(email, password).subscribe(data => {
          if (data.userToken.length > 0) {
            this.userService.user.userEmail = data.userEmail;
            this.userService.user.userToken = data.userToken;
            this.userService.user._id = data._id;
            this.userService.user.userType = data.userType;
            this.userService.user.userDepartment = data.userDepartment;
            this.userService.user.isUserAuthenticated = true;
            const expiresAt = moment().add(data.expiresInMinutesJWT, 'day');
            localStorage.setItem('token', JSON.stringify(data.userToken).slice(1, -1));
            localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
            switch (data.userType) {
              case enumUserTypes.user:
                this.router.navigate(['account/orders-list']);
                break;
              default:
                this.router.navigate(['/']);
            }
            this.orderService.resetOrder();
            this.haveError = false;
            this.authenticationAfterOrder = false;
          }
        }, error => {
          if (error.status === 401) {
            this.errorText = error.error.message;
            this.haveError = true;
          } else {
            console.log(error);
          }
        }
      );
    } else {
      this.getToken(email, password).subscribe(data => {
          if (data.userToken.length > 0) {
            this.userService.user.userEmail = data.userEmail;
            this.userService.user.userToken = data.userToken;
            this.userService.user.userName = data.userName;
            this.userService.user.userDepartment = data.userDepartment;
            this.userService.user._id = data._id;
            this.userService.user.userType = data.userType;
            this.userService.user.isUserAuthenticated = true;
            const expiresAt = moment().add(data.expiresInMinutesJWT, 'day');
            localStorage.setItem('token', JSON.stringify(data.userToken).slice(1, -1));
            localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));

            switch (data.userType) {
              case enumUserTypes.user:
                this.router.navigate(['account/orders-list']);
                break;
              default:
                this.router.navigate(['/']);
            }
            this.haveError = false;
          }
        }, error => {
          if (error.status === 401) {
            this.errorText = error.error.message;
            this.haveError = true;
          } else {
            console.log(error);
          }
        }
      );
    }
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  logout(): void {
    this.userService.user.userToken = '';
    this.userService.user.isUserAuthenticated = false;
    this.userService.user._id = null;
    this.userService.user.userType = enumUserTypes.guest;
    this.userService.user.userName = null;
    this.userService.user.userDepartment = null;
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    this.router.navigate(['/']);
  }

}
