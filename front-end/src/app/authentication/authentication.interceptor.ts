import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {UserService} from '../user/user.service';
import * as decode from 'jwt-decode';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(private userService: UserService) {
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    const idToken = localStorage.getItem('token');

    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', `Bearer ${idToken}`)
      });
      return next.handle(cloned).do(evt => {
        if (evt instanceof HttpResponse) {
          if (evt.status === 200 || evt.status === 201) {
            this.userService.user._id = decode(idToken).userId;
            this.userService.user.userEmail = decode(idToken).userEmail;
            this.userService.user.userType = decode(idToken).userType;
            this.userService.user.isUserAuthenticated = true;
          }
        }
      });
    } else {
      return next.handle(req);
    }

  }
}
