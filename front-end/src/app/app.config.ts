const mainRoute = 'http://localhost:8000';

export const apiRoutes = {
  registration: `${mainRoute}/api/registration`,
  registrationAfterOrder: `${mainRoute}/api/registration-after-order`,
  authentication: `${mainRoute}/api/authentication`,
  authenticationAfterOrder: `${mainRoute}/api/authentication-after-order`,
  orders: `${mainRoute}/api/orders`,
  openedOrders: `${mainRoute}/api/orders/opened`,
  closedOrders: `${mainRoute}/api/orders/closed`,
  canceledOrders: `${mainRoute}/api/orders/canceled`,
  userOpenedOrders: `${mainRoute}/api/user/orders/opened`,
  userClosedOrders: `${mainRoute}/api/user/orders/closed`,
  message: `${mainRoute}/api/messages/`,
  messages: `${mainRoute}/api/messages`,
  task: `${mainRoute}/api/tasks/`,
  tasks: `${mainRoute}/api/tasks`,
  notifications: `${mainRoute}/api/notifications`,
  fileUpload: `${mainRoute}/api/files`,
  fileDownload: `${mainRoute}/api/files`,
  employees: `${mainRoute}/api/employees`,
  clients: `${mainRoute}/api/clients`
};

