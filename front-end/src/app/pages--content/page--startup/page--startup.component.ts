import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page--startup',
  templateUrl: './page--startup.component.html',
  styleUrls: ['./page--startup.component.scss']
})
export class PageStartupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
