import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication/authentication.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-menu--main',
  templateUrl: './menu--main.component.html',
  styleUrls: ['./menu--main.component.scss']
})
export class MenuMainComponent implements OnInit {
  @Input() isUserAuthenticated: boolean;
  route: string;

  constructor(public authenticationService: AuthenticationService,
              public router: Router,
              public location: Location) {
    router.events.subscribe((val) => {
      if (location.path() !== '') {
        this.route = location.path();
      } else {
        this.route = '/';
      }
    });
  }

  ngOnInit() {

  }

}
