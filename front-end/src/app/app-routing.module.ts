import {NgModule, OnChanges} from '@angular/core';
import {RouterModule, Routes, CanActivate} from '@angular/router';
import {PageHomeComponent} from './pages--content/page--home/page--home.component';
import {PagePersonalComponent} from './pages--content/page--personal/page--personal.component';
import {PageBusinessComponent} from './pages--content/page--business/page--business.component';
import {PageStartupComponent} from './pages--content/page--startup/page--startup.component';
import {PageAboutComponent} from './pages--content/page--about/page--about.component';
import {PageAuthenticationComponent} from './pages--content/page--authentication/page--authentication.component';
import {PageRegistrationComponent} from './pages--content/page--registration/page--registration.component';
import {Page3ndflComponent} from './pages--lending/page--3ndfl/page--3ndfl.component';
import {EmployeesListComponent} from './account/employees-list/employees-list.component';
import {OrdersListComponent} from './account/orders-list/orders-list.component';
import {PaymentsListComponent} from './account/payments-list/payments-list.component';
import {SettingsComponent} from './account/settings/settings.component';
import {HelpComponent} from './account/help/help.component';
import {OrderComponent} from './order/order.component';
import {MessageComponent} from './message/message.component';
import {TaskComponent} from './task/task.component';
import {GuardService} from './guard/guard.service';
import {GuardRoleService} from './guard/guard-role.service';
import {enumUserTypes} from './user/user.model';
import {TaskListComponent} from './account/task-list/task-list.component';
import {ClientsListComponent} from './account/clients-list/clients-list.component';

const routes: Routes = [
  {
    path: 'account/create-user',
    component: PageRegistrationComponent,
    data: {
      state: 'account-create-user',
      expectedRole: enumUserTypes.root
    },
    canActivate: [GuardRoleService]
  },
  {
    path: 'account/create-task',
    component: TaskComponent,
    data: {
      state: 'account-create-task',
      expectedRole: [enumUserTypes.employee, enumUserTypes.manager, enumUserTypes.root],
    },
    canActivate: [GuardRoleService]
  },
  {
    path: 'account/users-list',
    component: EmployeesListComponent,
    data: {
      state: 'account-create-task',
      expectedRole: [enumUserTypes.employee, enumUserTypes.manager, enumUserTypes.root],
    },
    canActivate: [GuardRoleService]
  },
  {
    path: 'account/payments-list',
    component: PaymentsListComponent,
    canActivate: [GuardService]
  },
  {path: 'account/create-task', component: TaskComponent, canActivate: [GuardService]},
  {path: 'account/task/:taskId', component: TaskComponent, canActivate: [GuardService]},
  {path: 'account/tasks-list', component: TaskListComponent, data: {state: 'account-tasks-list'}, canActivate: [GuardService]},
  {path: 'account/order/:orderId/messages', component: MessageComponent, canActivate: [GuardService]},
  {path: 'account/order/:orderId', component: OrderComponent, canActivate: [GuardService]},
  {path: 'account/orders-list', component: OrdersListComponent, data: {state: 'account-orders-list'}, canActivate: [GuardService]},
  {path: 'account/employees-list', component: EmployeesListComponent, data: {state: 'account-employees-list'}, canActivate: [GuardService]},
  {path: 'account/clients-list', component: ClientsListComponent, data: {state: 'account-clients-list'}, canActivate: [GuardService]},
  {path: 'account/help', component: HelpComponent, data: {state: 'account-help'}, canActivate: [GuardService]},
  {path: 'account', component: SettingsComponent, data: {state: 'account'}, canActivate: [GuardService]},
  {path: '3ndfl', component: Page3ndflComponent, data: {state: '3ndfl'}},
  {path: 'registration', component: PageRegistrationComponent, data: {state: 'registration'}},
  {path: 'login', component: PageAuthenticationComponent, data: {state: 'login'}},
  {path: 'personal', component: PagePersonalComponent, data: {state: 'personal'}},
  {path: 'business', component: PageBusinessComponent, data: {state: 'business'}},
  {path: 'startup', component: PageStartupComponent, data: {state: 'startup'}},
  {path: 'about', component: PageAboutComponent, data: {state: 'about'}},
  {path: '', component: PageHomeComponent, data: {state: 'home'}}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
