import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import morgan from 'morgan';
import morganBody from 'morgan-body';
import passport from 'passport';

import {database, secretWord} from './config.js';
import User from './models/user';
import Notification from './models/notification';
import mongoose from './mongoose.config';
import routes from './routes';


const app = express();


// passport and JWT settings
app.set('secretWord', secretWord);

// mongodb connection
const db = mongoose.connect(database, {useMongoClient: true});
// Create notification collection in first initialization. Need for one notification store for all users
db.on('connected', function () {
  Notification.count(function (err, count) {
    if (!err && count === 0) {
      const notificationData = {
        ordersNew: [{}],
        messagesNew: [{}],
        paymentsNew: [{}],
        helpsNew: [{}],
        tasksNew: [{}]
      };
      Notification.create(notificationData, function (err) {
        if (err) {
          return next(err);
        }
      })
    }
  })
});


// mongo error
db.on('error', console.error.bind(console, 'connection error:'));


// parse incoming requests
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));


// use passport js for authenticate
app.use(passport.initialize());
app.use(passport.session());


// userPassword sessions
passport.serializeUser(function (user, done) {
  done(null, user._id);
});


passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});


// use morgan to log requests to the console. WARNING! It should be 
// before app use routes
app.use(morgan('dev'));
morganBody(app, {maxBodyLength: 100000});


// TODO:  Write normal cors domain for production!!!
//create a cors middleware

// user routes
// user routes
export const corsOptions = {
  origin: 'http://localhost:4200',
};
// export const corsOptionsFiles = {
//   origin: 'http://localhost:4200',
//   allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept']
// };
app.options('*', cors(corsOptions));
app.use('/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('404. Not found');
  err.status = 404;
  next(err);
});


// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
  res.status(err.status || 500).json({
    error: err.message
  });
});


// listen on port 3000
app.listen(8000, () => console.log('App running on http://localhost:8000/'));


