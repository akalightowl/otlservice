import cors from 'cors';
import express from 'express';
import jwt from 'jsonwebtoken';
import multer from 'multer';
import * as path from 'path';
import passport from 'passport';
import passportJwt from 'passport-jwt';
import {corsOptions, corsOptionsFiles} from '../app';
import {secretWord, fileUploadDir} from '../config.js';
import Message from '../models/message';
import Order from '../models/order';
import User from '../models/user';
import Task from '../models/task';
import Notification from '../models/notification';
import * as fs from 'fs';

const router = express.Router();


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// SETTING JWT ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = secretWord;
jwtOptions.expiresInMinutesJWT = 1440;

passport.use(new JwtStrategy(jwtOptions,
  function (jwt_payload, done) {
    User.findOne({userEmail: jwt_payload.userEmail}, function (err, user) {
      if (err) {
        return done(err, false);
      }
      if (user) {
        return done(null, user);
      } else {
        return done(null, false);
        // or you could create a new account
      }
    });
  }
));


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// SETTING MULTER ---------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

const maxSize = 1 * 10000 * 10000;
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // TODO: Don't foreg create folder after deploy
    cb(null, fileUploadDir);
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});


const upload = multer({
  storage: storage,
  limits: {fileSize: maxSize},
  fileFilter: function (req, file, cb) {
    if (!approvedFileMimeTypes.includes(file.mimetype)) {

      req.fileValidationError = file.mimetype + 'Неверный тип файла. Пожалуйста, ознакомтесь со списком доступных расширений файлов или обратитесь в службу поддержки.';
      return cb(null, false, new Error('Неверный тип файла. Пожалуйста, ознакомтесь со списком доступных расширений файлов или обратитесь в службу поддержки.'));
    }
    cb(null, true);
  }
}).single('file');

const approvedFileMimeTypes = [
  'image/gif',
  'image/jpeg',
  'image/pjpeg',
  'image/png',
  'image/tiff',
  'text/csv',
  'text/plain',
  'application/msword',
  'application/octet-stream',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/vnd.oasis.opendocument.text',
  'application/vnd.oasis.opendocument.spreadsheet',
  'application/vnd.oasis.opendocument.presentation',
  'application/vnd.oasis.opendocument.graphics',
  'application/pdf'
];

//   '.bmp', '.BMP',
//   '.png', '.PNG',
//   '.jpg', '.JPG',
//   '.jpeg', '.JPEG',
//   '.gif', '.GIF',
//   '.tif', '.TIF',
//   '.tiff', '.TIFF',
//   '.pdf', '.PDF',
//   '.odt', '.ODT',
//   '.ods', '.ODS',
//   '.odp', '.ODP',
//   '.doc', '.DOC',
//   '.docx', '.DOCX',
//   '.xls', '.XLS',
//   '.xlsx', '.XLSX',
//   '.ppt', '.PPT',
//   '.pptx', '.PPTX',
//   '.txt', '.TXT',
//   '.rtf', '.RFT',


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// SIMPLE ROUTES -------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


router.get('/', (req, res, next) => {
  return res.send('Otlservice. Homepage.')
});

router.get('/about', (req, res, next) => {
  return res.send('Otlservice. About.')
});


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// API ROUTES ----------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// USER'S API ----------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// POST /api/registration
// Registration

router.post('/api/registration', cors(corsOptions), function (req, res, next) {
  if (req.body.userEmail &&
    req.body.userPassword) {
    // use schema's `create` method to insert document into MongoDB
    User.create({
        userEmail: req.body.userEmail,
        userPassword: req.body.userPassword,
        userType: req.body.userType,
        userName: req.body.userName,
        userDepartment: req.body.userDepartment
      },
      function (err, user) {
        if (err) {
          if (err.status = 409) {
            res.status(err.status).json({message: 'Пользователь с таким email адресом уже зарегистрирован. Воспользуйтесь формой восстановления пароля, если Вы позабыли пароль от Вашего аккаунта.'})
          } else {
            next(err);
          }
        } else {
          let token = jwt.sign({
              userId: user._id,
              userEmail: user.userEmail,
              userType: user.userType
            },
            jwtOptions.secretOrKey);
          // return the information including token as JSON
          res.status(201).json({
            _id: user._id,
            userEmail: user.userEmail,
            userType: user.userType,
            userToken: token,
            userOrders: user.userOrders,
            userName: user.userName,
            userDepartment: user.userDepartment,
            userTasksCreator: user.userTasksCreator,
            userTasksExecutor: user.userTasksExecutor
          });
        }
      })

  }
  else {
    res.status(400).json({message: 'All fields required.'})
  }
});

// POST /api/registration-after-order
// Registration after creating order

router.post('/api/registration-after-order', cors(corsOptions), function (req, res, next) {
  if (req.body.userEmail &&
    req.body.userPassword) {
    // use schema's `create` method to insert document into MongoDB
    User.create({
        userEmail: req.body.userEmail,
        userPassword: req.body.userPassword,
        userType: req.body.userType
      },
      function (err, user) {
        if (err) {
          if (err.status = 409) {
            res.status(err.status).json({message: 'Пользователь с таким email адресом уже зарегистрирован. Воспользуйтесь формой восстановления пароля, если Вы позабыли пароль от Вашего аккаунта.'})
          } else {
            next(err);
          }
        } else {
          Order.create({
              userId: user._id,
              orderName: req.body.orderName,
              orderCost: req.body.orderCost,
              isOrderOpen: req.body.isOrderOpen,
              isOrderCancel: req.body.isOrderCancel,
              isOrderPaidOf: req.body.isOrderPaidOf,
              orderWorkStatus: req.body.orderWorkStatus,
              orderPaymentStatus: req.body.orderPaymentStatus,
              orderDeadLine: req.body.orderDeadLine,
              orderDataOpened: req.body.orderDataOpened,
              orderDataUpdate: req.body.orderDataUpdate,
              orderDataClosed: req.body.orderDataClosed,
              orderDataPayment: req.body.orderDataPayment,
              orderNotViewed: req.body.orderNotViewed,
              orderMessages: req.body.orderMessages,
              haveNotReadMessages: req.body.haveNotReadMessages,
              orderContent: req.body.orderContent
            },
            function (err, order) {
              if (err) {
                return next(err);
              } else {
                // Add +1 order to notification and orderId to new order's array.
                Notification.update({}, {
                  "$push": {"ordersNew": order._id}
                }, function (err, notification) {
                  if (err) {
                    return next(err);
                  } else {
                    // Add new orderId to user's userOrders array
                    User.findByIdAndUpdate(order.userId, {"$push": {"userOrders": order._id}}, function (err) {
                      if (err) {
                        return res.send(err);
                      }
                      let token = jwt.sign({
                          userId: user._id,
                          userEmail: user.userEmail,
                          userType: user.userType
                        },
                        jwtOptions.secretOrKey);
                      // return the information including token as JSON
                      res.status(201).json({
                        _id: user._id,
                        userEmail: user.userEmail,
                        userType: user.userType,
                        userToken: token,
                        userOrders: user.userOrders,
                        userTasksCreator: user.userTasksCreator,
                        userTasksExecutor: user.userTasksExecutor
                      });
                    });
                  }
                });
              }
            });
        }
      })

  }
  else {
    res.status(400).json({message: 'All fields required.'})
  }
});


// POST /api/authentication
// Authentication

router.post('/api/authentication', cors(corsOptions), function (req, res) {
  // find the user
  User.findOne({
    userEmail: req.body.userEmail
  }, function (err, user) {

    if (!user) {
      res.status(401).json({success: false, message: 'Пользователь с таким email не зарегистрирован.'});
    } else if (user) {
      // check if userPassword matches
      if (!user.validPassword(req.body.userPassword)) {
        res.status(401).json({success: false, message: 'Пожалуйста, введите правильный пароль.'});
      } else {
        // if user is found and userPassword is right
        // create a token with only our given payload
        // we don't want to pass in the entire user since that has the userPassword
        let token = jwt.sign({
            userId: user._id,
            userEmail: user.userEmail,
            userType: user.userType,
          },
          jwtOptions.secretOrKey);
        // return the information including token as JSON
        res.status(200).json({
          _id: user._id,
          userToken: token,
          expiresInMinutesJWT: jwtOptions.expiresInMinutesJWT,
          userType: user.userType,
          userEmail: user.userEmail,
          userName: user.userName
        });
      }
    }
  });
});


// POST /api/authentication-after-order
// Authentication user and create order

router.post('/api/authentication-after-order', cors(corsOptions), function (req, res) {
  // find the user
  User.findOne({
    userEmail: req.body.userEmail
  }, function (err, user) {

    if (!user) {
      res.status(401).json({success: false, message: 'Пользователь с таким email не зарегистрирован.'});
    } else if (user) {
      // check if userPassword matches
      if (!user.validPassword(req.body.userPassword)) {
        res.status(401).json({success: false, message: 'Пожалуйста, введите правильный пароль.'});
      } else {
        Order.create({
            userId: user._id,
            orderName: req.body.orderName,
            orderCost: req.body.orderCost,
            isOrderOpen: req.body.isOrderOpen,
            isOrderCancel: req.body.isOrderCancel,
            isOrderPaidOf: req.body.isOrderPaidOf,
            orderWorkStatus: req.body.orderWorkStatus,
            orderPaymentStatus: req.body.orderPaymentStatus,
            orderDeadLine: req.body.orderDeadLine,
            orderDataOpened: req.body.orderDataOpened,
            orderDataUpdate: req.body.orderDataUpdate,
            orderDataClosed: req.body.orderDataClosed,
            orderDataPayment: req.body.orderDataPayment,
            // TODO: change to real data
            orderNotViewed: req.body.orderNotViewed,
            orderMessages: req.body.orderMessages,
            haveNotReadMessages: req.body.haveNotReadMessages,
            orderContent: req.body.orderContent
          },
          function (err, order) {
            if (err) {
              return res.send(err);
            } else {
              // Add +1 order to notification and orderId to new order's array.
              Notification.update({}, {
                "$push": {"ordersNew": order._id}
              }, function (err, notification) {
                if (err) {
                  return res.send(err);
                } else {
                  // Add new orderId to user's userOrders array
                  User.findByIdAndUpdate(order.userId, {"$push": {"userOrders": order._id}}, function (err) {
                    if (err) {
                      return res.send(err);
                    }
                    let token = jwt.sign({
                        userId: user._id,
                        userEmail: user.userEmail,
                        userType: user.userType
                      },
                      jwtOptions.secretOrKey);
                    // return the information including token as JSON
                    res.status(201).json({
                      _id: user._id,
                      userEmail: user.userEmail,
                      userType: user.userType,
                      userToken: token,
                      userOrders: user.userOrders,
                      userTasksCreator: user.userTasksCreator,
                      userTasksExecutor: user.userTasksExecutor
                    });
                  });
                }
              });
            }
          });
      }
    }
  });
});


// GET /api/users
// Get all users.

router.get('/api/users', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.find({}, function (err, users) {
    res.status(200).json(users);
  });
});

// GET /api/employees
// Get all employees

router.get('/api/employees', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.find({$or: [{userType: 'employee'}, {userType: 'manager'}, {userType: 'root'}]}, function (err, employee) {
    res.status(200).json(employee);
  });
});

// GET /api/clients
// Get all users without employees

router.get('/api/clients', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.find({$or: [{userType: 'user'}]}, function (err, clients) {
    res.status(200).json(clients);
  });
});

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ORDERS'S API --------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// GET /api/orders
// Get all orders

router.get('/api/orders', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.find({}, function (err, orders) {
    res.status(200).json(orders);
  });
});

// GET /api/orders/opened
// Get all opened orders.

router.get('/api/orders/opened', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.find({isOrderOpen: true}, function (err, orders) {
    res.status(200).json(orders);
  });
});

// GET /api/orders/canceled
// Get all canceled orders.

router.get('/api/orders/canceled', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.find({isOrderCancel: true}, function (err, orders) {
    res.status(200).json(orders);
  });
});

// GET /api/orders/opened
// Get all closed orders.

router.get('/api/orders/closed', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.find({isOrderOpen: false, isOrderCancel: false}, function (err, orders) {
    res.status(200).json(orders);
  });
});

// POST /api/orders
// Create order

router.post('/api/orders', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.create({
      userId: req.body.userId,
      orderName: req.body.orderName,
      orderCost: req.body.orderCost,
      isOrderOpen: req.body.isOrderOpen,
      isOrderCancel: req.body.isOrderCancel,
      isOrderPaidOf: req.body.isOrderPaidOf,
      orderWorkStatus: req.body.orderWorkStatus,
      orderPaymentStatus: req.body.orderPaymentStatus,
      orderDeadLine: req.body.orderDeadLine,
      orderDataOpened: req.body.orderDataOpened,
      orderDataUpdate: req.body.orderDataUpdate,
      orderDataClosed: req.body.orderDataClosed,
      orderDataPayment: req.body.orderDataPayment,
      orderNotViewed: req.body.orderNotViewed,
      orderMessages: req.body.orderMessages,
      haveNotReadMessages: req.body.haveNotReadMessages,
      orderContent: req.body.orderContent
    },
    function (err, order) {
      if (err) {
        return next(err);
      } else {
        // Add +1 order to notification and orderId to new order's array.
        Notification.update({}, {
          "$push": {"ordersNew": order._id}
        }, function (err, notification) {
          if (err) {
            return next(err);
          } else {
            // Add new orderId to user's userOrders array
            User.findByIdAndUpdate(order.userId, {"$push": {"userOrders": order._id}}, function (err) {
              if (err) {
                return res.send(err);
              }
              res.status(201).json({message: `Create ${order._id} order successful.`});
            });
          }
        });

      }
    })
});

// POST /api/user/orders/opened
// Get user orders. Use post for getting userId without saving it in URL history.

router.post('/api/user/orders/opened', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.findById(req.body._id,
    function (err, user) {
      Order.find({isOrderOpen: true}).where('_id').in(user.userOrders).exec(function (err, orders) {
        if (err) {
          return res.send(err);
        }
        res.status(200).json(orders);
      });
    });
});

// POST /api/user/orders/closed
// Get user orders. Use post for getting userId without saving it in URL history.

router.post('/api/user/orders/closed', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.findById(req.body._id,
    function (err, user) {
      Order.find({isOrderOpen: false}).where('_id').in(user.userOrders).exec(function (err, orders) {
        if (err) {
          return res.send(err);
        }
        res.status(200).json(orders);
      });
    });
});

// GET /api/orders/:order_id
// GET /api/orders/:order_id
// GET order detail

router.get('/api/orders/:order_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findById(req.params.order_id, function (err, order) {
    if (err) {
      return res.send(err);
    }
    res.status(200).json(order);
  });
});

// PUT /api/orders/:order_id
// Update order detail

router.put('/api/orders/:order_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findByIdAndUpdate(req.params.order_id,
    {
      orderDataUpdate: req.body.orderDataUpdate,
      orderContent: req.body.orderContent
    },
    function (err) {
      if (err) {
        return res.send(err);
      }
      // Second request to base need, fot getting update information about order.
      Order.findById(req.params.order_id, function (err, order) {
        res.status(201).json(order);
      })

    });
});

// PUT /api/orders/:order_id/mark-as-read
// Mark order as read order detail

router.put('/api/orders/:order_id/mark-as-read', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findByIdAndUpdate(req.params.order_id,
    {
      orderNotViewed: false
    },
    function (err, order) {
      if (err) {
        return res.send(err);
      }
      Notification.findOneAndUpdate({}, {
        "$pull": {ordersNew: order._id}
      }, function (err) {
        if (err) {
          return res.send(err);
        }
      });
      // Second request to base need, fot getting update information about order.
      Order.findById(req.params.order_id, function (err, order) {
        res.status(201).json(order);
      })
    });
});

// PUT /api/orders/:order_id/cancel
// Cancel order. Not use delete http, because we don't delete any order. Only cancel and hide.
// TODO: Sometimes have problem with Header's. Maybe have double sending.

router.put('/api/orders/:order_id/cancel', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findByIdAndUpdate(req.params.order_id, {
      isOrderOpen: req.body.isOrderOpen,
      isOrderCancel: req.body.isOrderCancel
    },
    function (err, order) {
      if (err) {
        return res.send(err);
      }
      Notification.findOneAndUpdate({}, {
        "$pull": {ordersNew: order._id}
      }, function (err) {
        if (err) {
          return res.send(err);
        }
        res.status(201).json(order);
      });

    });
});

// PUT /api/orders/:order_id/close
// Close order. Not use delete http, because we don't delete any order. Only close and hide.

router.put('/api/orders/:order_id/close', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findByIdAndUpdate(req.params.order_id, {
      isOrderOpen: req.body.isOrderOpen
    },
    function (err, order) {
      if (err) {
        return res.send(err);
      }
      Notification.findOneAndUpdate({}, {
        "$pull": {ordersNew: order._id}
      }, function (err) {
        if (err) {
          return res.send(err);
        }
        res.status(201).json(order);
      });
    });
});

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// MESSAGES'S API ------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// GET /api/messages
// Get all messages

router.get('/api/messages', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Message.find({}, function (err, messages) {
    res.status(200).json(messages);
  });
});

// POST /api/messages
// Create message

router.post('/api/messages', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Message.create({
      userId: req.body.userId,
      orderId: req.body.orderId,
      isRead: req.body.isRead,
      messageContent: req.body.messageContent,
      messageCreatorType: req.body.messageCreatorType,
      messageDataCreated: req.body.messageDataCreated
    },
    function (err, message) {
      if (err) {
        return next(err);
      } else {
        User.findByIdAndUpdate(message.userId, {"$push": {"userMessages": message._id}}, function (err) {
          if (err) {
            return res.send(err);

          }
        });
        Order.findByIdAndUpdate(message.orderId, {
          "$push": {"orderMessages": message._id}
        }, function (err) {
          if (err) {
            return res.send(err);
          }
        });
        res.status(201).json({message: `Create ${message._id} message successful.`});
      }
    })
});

// GET /api/messages/:messages_id
// GET order message

router.get('/api/messages/:messages_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Message.findById(req.params.messages_id, function (err, message) {
    res.status(200).json(message);
  });
});

// PUT /api/messages/:messages_id
// PUT order message

router.put('/api/messages/:messages_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Message.findByIdAndUpdate(req.params.messages_id,
    {
      userId: req.body.userId,
      orderId: req.body.orderId,
      isRead: req.body.isRead,
      messageContent: req.body.messageContent,
      messageCreatorType: req.body.messageCreatorType,
      messageDataCreated: req.body.messageDataCreated
    },
    function (err, message) {
      res.status(201).json({message: `Update ${message._id} message successful.`});
    });
});


// DELETE /api/messages/:messages_id
// DELETE order message

router.delete('/api/messages/:messages_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Message.findByIdAndRemove(req.params.messages_id, function (err, message) {
    res.status(200).json({message: message});
  });
});


// GET /api/orders/:order_id/messages
// GET order messages

router.get('/api/orders/:order_id/messages', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Order.findById(req.params.order_id, function (err, order) {
    Message.find().where('_id').in(order.orderMessages).exec(function (err, messages) {
      res.status(200).json({"orderName": order.orderName, "orderId": order._id, "messagesList": messages});
    });
  });
});


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// TASK'S API ----------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// GET /api/tasks
// Get all tasks

router.get('/api/tasks', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Task.find({}, function (err, tasks) {
    res.status(200).json(tasks);
  });
});

// GET /api/tasks/:user_id/executor
// GET tasks for this user

router.get('/api/tasks/:user_id/executor', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.findById(req.params.user_id, function (err, user) {
    Task.find().where('_id').in(user.userTasksExecutor).exec(function (err, tasks) {
      res.status(200).json({"user": user._id, "userTasksExecutor": user.userTasksExecutor, "tasks": tasks});
    });
  });
});

// GET /api/tasks/:user_id/creator
// GET tasks crete by this user

router.get('/api/tasks/:user_id/creator', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  User.findById(req.params.user_id, function (err, user) {
    Task.find().where('_id').in(user.userTasksCreator).exec(function (err, tasks) {
      res.status(200).json({"user": user._id, "userTasksCreator": user.userTasksCreator, "tasks": tasks});
    });
  });
});

// GET /api/tasks/:task_id
// GET tasks by ID

router.get('/api/tasks/:task_id', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Task.findById(req.params.task_id, function (err, task) {
    // Add status for ngInit checking task on frontend. If request with task_id- send task, no send 503, fot create-task url
    task ? res.status(200).json({task: task, status: 200}) : res.status(503).json({status: 503})
  });
});


// POST /api/tasks
// Create task

router.post('/api/tasks', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Task.create({
      taskName: req.body.taskName,
      taskCreatorName: req.body.taskCreatorName,
      taskCreatorId: req.body.taskCreatorId,
      taskExecutorId: req.body.taskExecutorId,
      taskTargetUserId: req.body.taskTargetUserId,
      taskTargetOrderId: req.body.taskTargetOrderId,
      taskTargetOrderName: req.body.taskTargetOrderName,
      taskPriority: req.body.taskPriority,
      taskStatus: req.body.taskStatus,
      isTaskOpen: req.body.isTaskOpen,
      taskDataOpened: req.body.taskDataOpened,
      taskDeadline: req.body.taskDeadline,
      taskDataUpdate: req.body.taskDataUpdate,
      taskDataClosed: req.body.taskDataClosed,
      taskContent: req.body.taskContent
    },
    function (err, task) {
      if (err) {
        return next(err);
      } else {
        User.findByIdAndUpdate(task.taskCreatorId, {"$push": {"userTasksCreator": task._id}}, function (err) {
          if (err) {
            return res.send(err);
          }
          User.findByIdAndUpdate(task.taskExecutorId, {"$push": {"userTasksExecutor": task._id}}, function (err) {
            if (err) {
              return res.send(err);
            }
            res.status(201).json({task: `Create ${task._id} task successful.`});
          });
        });
      }
    })
});




// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// NOTIFICATION'S API ----------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// GET /api/notifications
// Get notifications

router.get('/api/notifications', cors(corsOptions), passport.authenticate('jwt', {session: false}), function (req, res, next) {
  Notification.findOne({}, function (err, notifications) {
    res.status(200).json(notifications);
  });
});


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// FILE'S OPERATION API ----------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


// POST /api/files
// Upload file

router.post('/api/files', cors(corsOptions), function (req, res) {
  upload(req, res, function (err) {
    if (req.fileValidationError) {
      return res.status(400).json({message: req.fileValidationError});
    }
    return res.status(201).json({
      message: `Файл загружен.`,
      fileUrl: `/uploads/${req.file.filename}`
    });
  })
});

// GET /api/files
// Download file

router.get('/api/files/:fileUrl', cors(corsOptions), function (req, res, next) {
  return res.download(`${fileUploadDir}/${req.params.fileUrl}`);
});


// Delete /api/files
// Delete file. Use put for change user files array.

router.put('/api/files/:fileName', cors(corsOptions), function (req, res, next) {
  Order.findByIdAndUpdate(req.params.order_id,
    {
      orderContent: req.body.orderContent
    },
    function (err, order) {
      if (err) {
        return res.send(err);
      }
      fs.unlink(`${fileUploadDir}/${req.params.fileName}`, function (err) {
        if (err) {
          return next(err);
        }
        return res.status(204).json({status: 204, message: 'Файл удален успешно'});
      });
    });
});


// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------


module.exports = router;