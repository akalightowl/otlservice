import mongoose from '../mongoose.config';

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const OrderSchema = new mongoose.Schema({
    userId: {
      type: ObjectId,
      required: true,
    },
    orderName: {
      type: String,
      required: true,
    },
    orderCost: {
      type: Number,
      required: true
    },
    isOrderOpen: {
      type: Boolean,
      required: true
    },
    isOrderCancel: {
      type: Boolean,
      required: true
    },
    isOrderPaidOf: {
      type: Boolean,
      required: true,
    },
    orderWorkStatus: {
      type: String,
      lowercase: true,
      required: true,
      trim: true
    },
    orderPaymentStatus: {
      type: String,
      lowercase: true,
      required: true,
      trim: true
    },
    orderDeadLine: {
      type: String,
      required: true
    },
    orderDataOpened: {
      type: Date,
      required: true
    },
    orderDataUpdate: {
      type: Date
    },
    orderDataClosed: {
      type: Date,
    },
    orderDataPayment: {
      type: Date,
    },
    orderNotViewed: {
      type: Boolean,
      required: true
    },
    orderMessages: {
      type: Array,
      required: true
    },
    haveNotReadMessages: {
      type: Boolean,
      required: true
    },
    orderContent: {
      type: Array,
      required: true
    }
  },
  {
    runSettersOnQuery: true
  }
  )
;

const Order = mongoose.model('Order', OrderSchema);
module.exports = Order;