import mongoose from '../mongoose.config';

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const NotificationSchema = new mongoose.Schema({
    ordersNew: {
      type: Array,
      required: true
    },
    messagesNew: {
      type: Array,
      required: true
    },
    paymentsNew: {
      type: Array,
      required: true
    },
    helpsNew: {
      type: Array,
      required: true
    },
    tasksNew: {
      type: Array,
      required: true
    }
  },
  {
    runSettersOnQuery: true
  }
  )
;

const Notification = mongoose.model('Notification', NotificationSchema);
module.exports = Notification;