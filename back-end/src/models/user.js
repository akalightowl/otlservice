import bcrypt from 'bcrypt';
import mongoose from '../mongoose.config';

const UserSchema = new mongoose.Schema({
  userEmail: {
    type: String,
    lowercase: true,
    unique: true,
    required: true,
    trim: true
  },
  userPassword: {
    type: String,
    required: true
  },
  userType: {
    type: String,
    enum: [
      'guest',
      'user',
      'employee',
      'manager',
      'root'],
    default: 'guest',
    required: true
  },
  userName: {
    type: String
  },
  userDepartment: {
    type: String
  },
  userOrders: {
    type: Array,
    default: [{}]
  },
  userTasksCreator: {
    type: Array,
    default: [{}]
  },
  userTasksExecutor: {
    type: Array,
    default: [{}]
  }
}, {runSettersOnQuery: true});


UserSchema.methods.validPassword = function (password) {
  // TODO: rewrite this part with async bcrypt.compare for better performance
  return bcrypt.compareSync(password, this.userPassword);
};


// hash userPassword before saving to database
UserSchema.pre('save', function (next) {
  let user = this;
  User.findOne({userEmail: user.userEmail}, function (err, exist) {
    if (exist) {
      const err = new Error('User with this userEmail already registered.');
      err.status = 409;
      return next(err);
    } else bcrypt.hash(user.userPassword, 10, function (err, hash) {
      if (err) {
        return next(err);
      }
      user.userPassword = hash;
      next();
    })
  });
});

const User = mongoose.model('User', UserSchema);
module.exports = User;