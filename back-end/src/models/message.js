import mongoose from '../mongoose.config';

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const MessageSchema = new mongoose.Schema({
    userId: {
      type: ObjectId,
      required: true,
    },
    orderId: {
      type: ObjectId,
      required: true,
    },
    isRead: {
      type: Boolean,
      required: true
    },
    messageContent: {
      type: String,
      required: true
    },
    messageCreatorType: {
      type: String,
      enum: [
        'guest',
        'user',
        'employee',
        'manager',
        'root'],
      default: 'user',
      required: true
    },
    messageDataCreated: {
      type: Date,
      // required: true
    }
  },
  {
    runSettersOnQuery: true
  }
  )
;

const Message = mongoose.model('Message', MessageSchema);
module.exports = Message;