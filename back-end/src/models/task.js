import mongoose from '../mongoose.config';

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const TaskSchema = new mongoose.Schema({
    taskName: {
      type: String,
      required: true
    },
    taskCreatorName: {
      type: String,
      required: true
    },
    taskCreatorId: {
      type: ObjectId,
      required: true,
    },
    taskExecutorId: {
      type: ObjectId,
      required: true,
    },
    taskTargetUserId: {
      type: ObjectId,
      required: true,
    },
    taskTargetOrderId: {
      type: ObjectId,
      required: true,
    },
    taskTargetOrderName: {
      type: String,
      required: true,
    },
    taskStatus: {
      type: String,
      required: true
    },
    taskPriority: {
      type: String,
      required: true
    },
    isTaskOpen: {
      type: Boolean,
      required: true
    },
    taskDataOpened: {
      type: Date,
      required: true
    },
    // TODO: Change to real Data type.
    taskDeadline: {
      type: String,
      required: true
    },
    taskDataUpdate: {
      type: Date,
    },
    taskDataClosed: {
      type: Date
    },
    taskContent: {
      type: String,
      required: true
    }
  },
  {
    runSettersOnQuery: true
  }
  )
;

const Task = mongoose.model('Task', TaskSchema);
module.exports = Task;