"use strict";

const babel = require('gulp-babel');
const clean = require('gulp-clean');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const plumberNotifier = require('gulp-plumber-notifier');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglifyes');

gulp.task('clean', () => {
  return gulp.src(['./dist'], { read: false })
    .pipe(clean())
});

gulp.task('minjs', function () {
  return gulp.src(['src/**/*.js'])
    .pipe(plumberNotifier())
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(uglify({
      mangle: false,
      ecma: 6
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/'));
});


gulp.task('default', function () {
  gulp.watch('src/**/*.js', ['minjs']);
}); 
